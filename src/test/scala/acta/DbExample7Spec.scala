package acta.db7


import java.sql.Connection

import acta.part2.db7._
import cats.Eq
import cats.laws.discipline.{ApplicativeTests, FunctorTests, MonadTests}
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.FunSuite
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.typelevel.discipline.scalatest.Discipline

private case class TestTransaction[T](val run: Connection => T)
  extends Transaction[T]

class DbExample7Spec extends FunSuite with Discipline with GeneratorDrivenPropertyChecks{
  implicit val transactionArb : Arbitrary[Transaction[Int]] =
    Arbitrary(
      Arbitrary
        .arbInt
        .arbitrary
        .map(n => TestTransaction(c => n)))

  implicit val transactionFAtoBArb : Arbitrary[Transaction[Int=>Int]] =
    Arbitrary(
      Arbitrary
        .arbInt
        .arbitrary
        .map(n => TestTransaction(c => (x => n))))

  implicit val connectionArb : Arbitrary[Connection] = Arbitrary(Gen.const(null))

  private def arbitraryValues[A : Arbitrary]: Stream[A] = Stream.continually(implicitly[Arbitrary[A]].arbitrary.sample).flatten

  protected def equalitySamplesCount: Int = 16

  implicit val eqInt : Eq[Int] = Eq.fromUniversalEquals[Int]

  implicit def eqTransaction[T] : Eq[Transaction[T]] = Eq.instance { (t1, t2) =>
    arbitraryValues[Connection].take(equalitySamplesCount).forall(c => t1.run(c) === t2.run(c))
  }

  checkAll("Functor Transaction", FunctorTests[Transaction].functor[Int, Int, Int])
  checkAll("Applicative Transaction", ApplicativeTests[Transaction].applicative[Int, Int, Int])
  checkAll("Monad Transaction", MonadTests[Transaction].stackUnsafeMonad[Int, Int, Int])
  //TODO checkAll("Monad Transaction", MonadTests[Transaction].monad[Int, Int, Int])
}
