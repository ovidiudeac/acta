package acta

import org.scalatest._
import org.scalatest.prop.PropertyChecks
import acta.part1.SerializableExtensions._

class SerializableSpec
  extends PropSpec
    with PropertyChecks
    with MustMatchers {
  property("serialize followed by deserialize returns the initial value") {
    forAll {i : Int =>
      i.serialize.deserialize mustBe i
    }
  }

  property("distinct values serialize to distinct strings") {
    forAll {(i : Int, j : Int) =>
      (i.serialize == j.serialize) mustBe (i == j)
    }
  }
}
