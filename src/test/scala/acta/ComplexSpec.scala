package acta

import cats.kernel.laws.discipline.MonoidTests
import org.scalatest.FunSuite
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.typelevel.discipline.scalatest.Discipline
import acta.part1.AggregateMonoidInstance.complexMonoid
import acta.part1.Complex
import cats.Eq
import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary.arbitrary

class ComplexSpec  extends FunSuite with Discipline with GeneratorDrivenPropertyChecks{
  implicit val complexArbitrary = Arbitrary(for {
    r <- arbitrary[Double]
    i <- arbitrary[Double]
  } yield Complex(r,i))

  implicit val complexEq : Eq[Complex] = (x: Complex, y: Complex) => x == y

  checkAll("Complex monoid", MonoidTests[Complex].monoid)
}
