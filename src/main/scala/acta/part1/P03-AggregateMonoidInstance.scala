package acta.part1

import cats.Monoid
import cats.implicits._

object AggregateMonoidInstance {
  def aggregate[T : Monoid](items: List[T]) : T = {
    val zero = implicitly[Monoid[T]].empty

    items.foldLeft (zero) ((a, x) => a.combine(x))
  }


  implicit val complexMonoid = new Monoid[Complex]{
    override def empty : Complex = Complex(0,0)

    override def combine(x: Complex, y: Complex): Complex = Complex(x.r+y.r, x.i + y.i)
  }

  def main() = {

    val complex = List(Complex(1,0), Complex(10,2), Complex(1,5))
    val r3 = aggregate(complex)
    println(s"Aggregated complex result: $r3\n")


  }

}
