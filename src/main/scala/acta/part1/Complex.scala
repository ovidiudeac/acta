package acta.part1

case class Complex(r: Double, i: Double) {
  override def toString =  s"$r + $i * i"
}
