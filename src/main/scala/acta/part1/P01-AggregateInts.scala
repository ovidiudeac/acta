package acta.part1

object AggregateInts {

  def aggregate(items: List[Int]) : Int =
    items.foldLeft (0) ((a, x) => a + x)



  def main() = {
    val ints = List(1,2,3,4,5,6,7,8,9)

    val r = aggregate(ints)

    println(s"Aggregated ints: $r\n")







//    TODO
//    val strings = List("a", "b", "c", "d", "e", "f")
//    val r2 = aggregate(strings)
//    println(s"Aggregated string result: $r2\n")



//    val complex = List(Complex(1,0), Complex(10,2), Complex(1,5))
//    val r3 = aggregate(complex)
//    println(s"Aggregated complex result: $r3\n")
  }

}
