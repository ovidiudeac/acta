package acta.part1

import cats.Monoid
import cats.implicits._

object AggregateGeneric {

  def aggregate[T : Monoid](items: List[T]) : T = {
    val initialAccumulator = implicitly[Monoid[T]].empty

    items.foldLeft (initialAccumulator) ((a, x) => a.combine(x))
  }


  def main() = {
    val ints = List(1,2,3,4,5,6,7,8,9)

    val r1 = aggregate(ints)

    println(s"Aggregated int result: $r1\n")

    val strings = List("a", "b", "c", "d", "e", "f")
    val r2 = aggregate(strings)
    println(s"Aggregated string result: $r2\n")








//    TODO
//    val complex = List(Complex(1,0), Complex(10,2), Complex(1,5))
//    val r3 = aggregate(complex)
//    println(s"Aggregated complex result: $r3\n")

  }
}
