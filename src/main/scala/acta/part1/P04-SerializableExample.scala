package acta.part1

trait Serializable[T] {
  def serialize(v: T) : String
  def deserialize(s:String) : T
}


object Serializable {
  implicit val serializableInt = new Serializable[Int]{
    override def serialize(a: Int): String = a.toString
    override def deserialize(s: String): Int = s.toInt
  }
}

object SerializableExample {
  import Serializable._

  def transform[T : Serializable](s : String, f : T => T) : String = {
    val serializableInst = implicitly[Serializable[T]]

    val v = serializableInst.deserialize(s)
    val r = f(v)

    serializableInst.serialize(r)
  }

  def main() = {
    val r = transform("1000", (i : Int) => i + 5000)

    println(s"SerializableExample: transform(1000)=$r\n")
  }
}



object SerializableExtensions {
  implicit class SerializableExtension[T : Serializable](v:T) {
    def serialize = implicitly[Serializable[T]].serialize(v)
  }

  implicit class DeserializableExtension(s:String) {
    def deserialize[T : Serializable] = implicitly[Serializable[T]].deserialize(s)
  }
}

object SerializableExample2 {
  import Serializable._
  import SerializableExtensions._

  def transform[T : Serializable](s : String, f : T => T) : String = {
    val v = s.deserialize[T]

    val r = f(v)

    r.serialize
  }

  def main() = {
    val r = transform("1000", (i : Int) => i + 5000)

    println(s"SerializableExample2: transform(1000)=$r\n")
  }
}