package acta.part2

import java.sql.Connection

import anorm.RowParser
import anorm.SqlParser.{long, str}
import scalikejdbc.ConnectionPool

object Db {
  Class.forName("org.postgresql.Driver")
  ConnectionPool.singleton("jdbc:postgresql://localhost/bankexample", "dev", "dev")



  def withConnection[A](block: Connection => A): A = {
    val connection = getConnection
    try {
      block(connection)
    } finally {
      connection.close()
    }
  }

  def getConnection : Connection = {
    val c = ConnectionPool.borrow()
    c.setAutoCommit(false)
    c
  }
}

case class Account ( id : String,
                     amount : Long
                   )

object Account {
  val rowParser: RowParser[Account] =
    for {
      id <- str("account_id")
      amount <- long("amount")
    } yield Account(id, amount)
}