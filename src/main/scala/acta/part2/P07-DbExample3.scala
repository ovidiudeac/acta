package acta.part2.db3

import java.sql.Connection

import acta.part2._
import anorm._


object AccountRepo {
  def execute[T](transaction: Connection => T) : T= {
    val connection = Db.getConnection
    try {
      val r = transaction(connection)
      connection.commit()
      r
    } finally {
      connection.close()
    }
  }
}

object AccountTable {
  def insert(account : Account) = {
    connection : Connection =>
      SQL("insert into accounts (account_id, amount) values ({account_id}, {amount})")
        .on(
          "account_id" -> account.id,
          "amount" -> account.amount)
        .execute()(connection)
  }

  def deleteAll = {
    connection : Connection =>
      SQL("delete from accounts").execute()(connection)
  }

  def listAll = {
    connection : Connection =>
      SQL("select * from accounts")
        .as(Account.rowParser.*)(connection)
  }

  private def updateAccount(accountId : String, accountTransaction : Long => Long) = {
    connection : Connection => {
      val accountOpt =
        SQL("select * from accounts where account_id={account_id}")
          .on("account_id" -> accountId)
          .as(Account.rowParser.*)(connection).headOption

      accountOpt match {
        case None => throw new RuntimeException(s"Account $accountId not found")
        case Some(account) =>
          SQL("update accounts set amount = {new_amount} where account_id={account_id}")
            .on(
              "account_id" -> accountId,
              "new_amount" -> accountTransaction(account.amount))
            .execute()(connection)
      }
    }
  }

  def deposit(accountId : String, sum : Long) = updateAccount(accountId, amount => amount + sum)
  def withdraw(accountId : String, sum : Long) = updateAccount(accountId, amount => amount - sum)
}

object Main {
  import AccountRepo.execute

  def main() = {
    println ("DbExample 3 -------------------------------")

    execute(AccountTable.deleteAll)

    println("Initial db: " + execute(AccountTable.listAll))

    execute(AccountTable.insert(Account("account1", 0)))
    execute(AccountTable.insert(Account("account2", 0)))

    println("After insert:" + execute(AccountTable.listAll))

    execute(AccountTable.deposit("account1", 100))
    execute(AccountTable.deposit("account2", 100))

     //execute(transaction("account1", "account2", 100))

    println("Final accounts: " + execute(AccountTable.listAll))
  }

  def transaction(from : String, to : String, amount : Long) = {
    ???
  }
}
