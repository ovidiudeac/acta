package acta.part2.db7

import java.sql.Connection

import acta.part2._
import anorm._
import cats.implicits._
import cats.{Applicative, Functor, Monad}

trait Transaction[+T] {
  val run: Connection => T

  def execute : T = {
    val connection = Db.getConnection
    try {
      val r = run(connection)
      connection.commit()
      r
    } finally {
      connection.close()
    }
  }
}

private case class TransactionImpl[T](val run: Connection => T)
  extends Transaction[T]


object Transaction {
  def update(sql: SimpleSql[Row]): Transaction[Int] =
    TransactionImpl(conn => sql.executeUpdate()(conn))

  def query[T](sql: SimpleSql[Row], rsp: ResultSetParser[T]): Transaction[T] =
    TransactionImpl(conn => sql.as(rsp)(conn))


  implicit val transactionFunctor: Functor[Transaction] =
    new Functor[Transaction] {
      def map[A, B](fa: Transaction[A])(f: A => B) =
        TransactionImpl(conn => {
          val v = fa.run(conn)
          f(v)
        })
    }

  implicit val transactionApplicative: Applicative[Transaction] =
    new Applicative[Transaction] {
      override def pure[A](x: A): Transaction[A] = TransactionImpl(c => x)

      override def ap[A, B](ff: Transaction[A => B])(
        fa: Transaction[A]): Transaction[B] =
        TransactionImpl(conn => {
          val f = ff.run(conn)
          val v = fa.run(conn)
          f(v)
        })
    }

  implicit val transactionMonad: Monad[Transaction] = new Monad[Transaction] {
    override def flatMap[A, B](fa: Transaction[A])(
      f: A => Transaction[B]): Transaction[B] =
      TransactionImpl(conn => {
        val x = fa.run(conn)
        f(x).run(conn)
      })

    //TODO: implement this as @tailrec
    // See https://github.com/purescript/purescript-tailrec/blob/master/src/Control/Monad/Rec/Class.purs
    override def tailRecM[A, B](a: A)(
      f: A => Transaction[Either[A, B]]): Transaction[B] =
      flatMap(f(a)) {
        case Left(a2) => tailRecM(a2)(f)
        case Right(b) => pure(b)
      }

    override def pure[A](x: A): Transaction[A] = transactionApplicative.pure(x)
  }

}

object AccountTable {
  def insert(account : Account) =
    Transaction.update(
      SQL("insert into accounts (account_id, amount) values ({account_id}, {amount})")
        .on(
          "account_id" -> account.id,
          "amount" -> account.amount))

  def deleteAll =
    Transaction.update(
      SQL("delete from accounts"))

  def listAll =
    Transaction.query(
      SQL("select * from accounts"),
      Account.rowParser.*)

  private def findAllById(accountId : String) : Transaction[List[Account]] =
    Transaction.query(
      SQL("select * from accounts where account_id={account_id}")
        .on("account_id" -> accountId),
      Account.rowParser.*)

  def find(accountId : String) : Transaction[Option[Account]] =
    findAllById(accountId).map(lst => lst.headOption)

  def updateById(accountId : String, amount : Long) : Transaction[Int] =
    Transaction.update(
      SQL("update accounts set amount = {new_amount} where account_id={account_id}")
        .on(
          "account_id" -> accountId,
          "new_amount" -> amount))

  private def update(accountId : String, accountTransaction : Long => Long) =
    for {
      accounts <- findAllById(accountId)

      changed <- accounts.headOption match {
          case None => throw new RuntimeException(s"Account $accountId not found")
          case Some(account) => updateById(accountId, accountTransaction(account.amount))
        }
      } yield changed

  def deposit(accountId : String, sum : Long) = update(accountId, amount => amount + sum)
  def withdraw(accountId : String, sum : Long) = update(accountId, amount => amount - sum)
}



object Main {

  def main() = {
    println ("DbExample 7 -------------------------------")

    AccountTable.deleteAll.execute

    println("Initial db: " + AccountTable.listAll.execute)

    AccountTable.insert(Account("account1", 0)).execute
    AccountTable.insert(Account("account2", 0)).execute

    println("After insert:" + AccountTable.listAll.execute)

    AccountTable.deposit("account1", 100).execute
    AccountTable.deposit("account2", 100).execute

    transaction("account1", "account2", 100).execute

    println("Final accounts: " + AccountTable.listAll.execute)
  }

  def transaction(from : String, to : String, amount : Long) : Transaction[Unit] =
    for {
      _ <- AccountTable.deposit(to, amount)
      _ <- AccountTable.withdraw(from, amount)
    } yield ()
}

