package acta.part2.db2

import java.sql.Connection

import acta.part2._
import anorm._


class AccountRepo {
  private var connection : Option[Connection] = None

  private def withConnection[T](f : Connection => T) : T =
    connection match {
      case None => throw new RuntimeException("No transaction is running")
      case Some(conn) => f(conn)
    }

  def startTransaction = {
    connection = Some(Db.getConnection)
  }

  def endTransaction = withConnection(c => {
    c.commit()
    c.close()
  })

  def insert(account : Account) = {
    withConnection(connection =>
      SQL("insert into accounts (account_id, amount) values ({account_id}, {amount})")
        .on(
          "account_id" -> account.id,
          "amount" -> account.amount)
        .execute()(connection))
  }

  def deleteAll = {
    withConnection(connection =>
      SQL("delete from accounts").execute()(connection))
  }

  def listAll = {
    withConnection(connection =>
      SQL("select * from accounts")
        .as(Account.rowParser.*)(connection))
  }

  private def updateAccount(accountId : String, accountTransaction : Long => Long) = {
    withConnection(connection => {
      val accountOpt =
          SQL("select * from accounts where account_id={account_id}")
            .on("account_id" -> accountId)
            .as(Account.rowParser.*)(connection).headOption

      accountOpt match {
        case None => throw new RuntimeException(s"Account $accountId not found")
        case Some(account) =>
          SQL("update accounts set amount = {new_amount} where account_id={account_id}")
            .on(
              "account_id" -> accountId,
              "new_amount" -> accountTransaction(account.amount))
            .execute()(connection)
      }
    })
  }

  def deposit(accountId : String, sum : Long) = updateAccount(accountId, amount => amount + sum)
  def withdraw(accountId : String, sum : Long) = updateAccount(accountId, amount => amount - sum)
}

object Main {

  def main() = {
    val accountRepo = new AccountRepo()

    accountRepo.startTransaction

    println ("DbExample 2 -------------------------------")

    accountRepo.deleteAll

    println("Initial db: " + accountRepo.listAll)

    accountRepo.insert(Account("account1", 0))
    accountRepo.insert(Account("account2", 0))

    println("After insert:" + accountRepo.listAll)

    accountRepo.deposit("account1", 100)
    accountRepo.deposit("account2", 100)

    // transaction("account1", "account2", 100)

    println("Final accounts: " + accountRepo.listAll)

    accountRepo.endTransaction
  }

  def transaction(from : String, to : String, amount : Long) = {
    ???
  }
}
