package acta.part2.db4

import java.sql.Connection

import acta.part2._
import anorm._

sealed trait Transaction[+T] {
  val run: Connection => T

  def execute : T = {
    val connection = Db.getConnection
    try {
      val r = run(connection)
      connection.commit()
      r
    } finally {
      connection.close()
    }
  }
}

private case class TransactionImpl[T](val run: Connection => T)
  extends Transaction[T]

object Transaction {
  def update(sql: SimpleSql[Row]): Transaction[Int] =
    TransactionImpl(conn => sql.executeUpdate()(conn))

  def query[T](sql: SimpleSql[Row], rsp: ResultSetParser[T]): Transaction[T] =
    TransactionImpl(conn => sql.as(rsp)(conn))
}

object AccountTable {
  def insert(account : Account) =
    Transaction.update(
      SQL("insert into accounts (account_id, amount) values ({account_id}, {amount})")
        .on(
          "account_id" -> account.id,
          "amount" -> account.amount))

  def deleteAll =
    Transaction.update(
      SQL("delete from accounts"))

  def listAll =
    Transaction.query(
      SQL("select * from accounts"),
      Account.rowParser.*)

  private def update(accountId : String, accountTransaction : Long => Long) = {
//        SQL("select * from accounts where account_id={account_id}")
//          .on("account_id" -> accountId)
//          .as(Account.rowParser.*)(connection).headOption
//
//      accountOpt match {
//        case None => throw new RuntimeException(s"Account $accountId not found")
//        case Some(account) =>
//          SQL("update accounts set amount = {new_amount} where account_id={account_id}")
//            .on(
//              "account_id" -> accountId,
//              "new_amount" -> accountTransaction(account.amount))
//            .execute()(connection)
//      }
//    }
    ???
  }

  def deposit(accountId : String, sum : Long) = update(accountId, amount => amount + sum)
  def withdraw(accountId : String, sum : Long) = update(accountId, amount => amount - sum)
}

object Main {

  def main() = {
    println ("DbExample 4 -------------------------------")

    AccountTable.deleteAll.execute

    println("Initial db: " + AccountTable.listAll.execute)

    AccountTable.insert(Account("account1", 0)).execute
    AccountTable.insert(Account("account2", 0)).execute

    println("After insert:" + AccountTable.listAll.execute)

//    execute(AccountTable.deposit("account1", 100))
//    execute(AccountTable.deposit("account2", 100))

    //execute(transaction("account1", "account2", 100))

    println("Final accounts: " + AccountTable.listAll.execute)
  }

  def transaction(from : String, to : String, amount : Long) = {
    ???
  }
}
