package acta.part2.db1

import acta.part2._
import anorm._


object AccountRepo {
  def insert(account : Account) = {
    val connection = Db.getConnection

    try {
      SQL("insert into accounts (account_id, amount) values ({account_id}, {amount})")
        .on(
          "account_id" -> account.id,
          "amount" -> account.amount)
          .execute()(connection)
      connection.commit()
    } finally {
      connection.close()
    }
  }

  def deleteAll = {
    val connection = Db.getConnection

    try {
      SQL("delete from accounts").execute()(connection)
      connection.commit()
    } finally {
      connection.close()
    }
  }

  def listAll = {
    val connection = Db.getConnection

    try {
      SQL("select * from accounts")
        .as(Account.rowParser.*)(connection)
    } finally {
      connection.close()
    }
  }

  private def updateAccount(accountId : String, accountTransaction : Long => Long) = {
    val connection = Db.getConnection

    try {
      val accountOpt =
        SQL("select * from accounts where account_id={account_id}")
          .on("account_id" -> accountId)
          .as(Account.rowParser.*)(connection).headOption

      accountOpt match {
        case None => throw new RuntimeException(s"Account $accountId not found")
        case Some(account) => {
          SQL("update accounts set amount = {new_amount} where account_id={account_id}")
            .on(
              "account_id" -> accountId,
              "new_amount" -> accountTransaction(account.amount))
            .execute()(connection)
          connection.commit()
        }
      }
    } finally {
      connection.close()
    }
  }

  def deposit(accountId : String, sum : Long) = updateAccount(accountId, amount => amount + sum)
  def withdraw(accountId : String, sum : Long) = updateAccount(accountId, amount => amount - sum)
}

object Main {

  def main() = {
    println ("DbExample 1 -------------------------------")

    AccountRepo.deleteAll

    println("Initial db: " + AccountRepo.listAll)

    AccountRepo.insert(Account("account1", 0))
    AccountRepo.insert(Account("account2", 0))

    println("After insert:" + AccountRepo.listAll)

    AccountRepo.deposit("account1", 100)
    AccountRepo.deposit("account2", 100)

   // transaction("account1", "account2", 100)

    println("Final accounts: " + AccountRepo.listAll)
  }

  def transaction(from : String, to : String, amount : Long) = {
    ???
  }
}
