import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.4",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "ACTA",
    libraryDependencies ++= Seq (
      "com.typesafe.play" %% "anorm" % "2.5.3",
      "org.postgresql" % "postgresql" % "9.4.1208",
      "org.typelevel" %% "cats-core" % "1.0.1",
      "org.typelevel" %% "cats-laws" % "1.0.1",
      "org.scalikejdbc" %% "scalikejdbc" % "3.2.2",
      scalaTest % Test,
      "com.ironcorelabs" %% "cats-scalatest" % "2.3.1" % Test,
      "org.typelevel" %% "discipline" % "0.8" % Test)
  )
